import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GetDate {

	/**
	 * This class is to check the date. This file is no need for Prediction Example.
	 * @author Isuru Madusanka.
	 * @see http://www.isuru.in
	 */
	public static void main(String[] args) {

		String dateFormat = "dd/MM/yyyy hh:mm:ss.SSS";
		long milliSeconds = 1347563889688l;

		DateFormat formatter = new SimpleDateFormat(dateFormat);

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		
		System.out.print(formatter.format(calendar.getTime()));

	}

}
